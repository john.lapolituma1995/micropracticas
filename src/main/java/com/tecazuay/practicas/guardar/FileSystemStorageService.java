package com.tecazuay.practicas.guardar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

@Service
public class FileSystemStorageService implements StorageService {

    private Path rootLocation;
    private String nombreArchivo;

    @Autowired
    public FileSystemStorageService(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
    }

    @Override
    public void changePath(String ruta) {
        this.rootLocation = Paths.get(ruta);
    }

    @Override
    public void setNombre(String nombre) {
        this.nombreArchivo = nombre;
    }

    @Override
    public String getPath() {
        return this.rootLocation.getFileName().toString();
    }

    @Override
    public void store(MultipartFile file) {
        try {
            if (file.isEmpty()) {
                throw new StorageException("Falló al intentar guardar el archivo " + file.getOriginalFilename());
            }
            Files.copy(file.getInputStream(), this.rootLocation.resolve(this.nombreArchivo));
            //Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()));
        } catch (IOException e) {
            throw new StorageException("Falló al intentar guardar el archivo " + file.getOriginalFilename(), e);
        }
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.rootLocation, 1)
                    .filter(path -> !path.equals(this.rootLocation))
                    .map(path -> this.rootLocation.relativize(path));
        } catch (IOException e) {
            throw new StorageException("Fallo al leer los archivos", e);
        }

    }

    @Override
    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new StorageFileNotFoundException("No puede leer el archivo: " + filename);

            }
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("No puede leer el archivo: " + filename, e);
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    @Override
    public void deleteOne(String file) {
        File f = new File(rootLocation.toString() + "/" + file);
        FileSystemUtils.deleteRecursively(f);
    }

    @Override
    public void init() {
        /*try {
            Files.createDirectory(rootLocation);
        } catch (IOException e) {
            throw new StorageException("No puede crear la carpeta", e);
        }*/
    }
}
