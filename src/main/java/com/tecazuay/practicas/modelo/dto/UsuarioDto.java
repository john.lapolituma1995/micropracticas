package com.tecazuay.practicas.modelo.dto;

import java.util.List;

import com.tecazuay.practicas.modelo.Perfil;

public class UsuarioDto {

    private String email;
 //   private String contrasena;
    private String usuario;
    private List<Perfil> perfil;
   
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

//    public String getContrasena() {
//        return contrasena;
//    }
//
//    public void setContrasena(String contrasena) {
//        this.contrasena = contrasena;
//    }
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

	public List<Perfil> getPerfil() {
		return perfil;
	}

	public void setPerfil(List<Perfil> perfil) {
		this.perfil = perfil;
	}

}
