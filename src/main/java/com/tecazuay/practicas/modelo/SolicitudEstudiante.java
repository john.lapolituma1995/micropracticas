package com.tecazuay.practicas.modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author HP
 */
@Entity
@Audited
@EntityListeners({ AuditingEntityListener.class })
public class SolicitudEstudiante implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idsolicitudEstudiante;
    private Date fechaInterna;
    private Date fecha;
    private String estado;
    private  int numeroEstudiantes;
    private  int horasRealizar;
    private String periodo;
    private String ciclo;
    private String paralelo;
     private String carrera;
    //relacion estudiante,convocatoria(listo)

     @CreatedDate
    Date creado;
    @LastModifiedDate
    Date actualizado;
   @ManyToOne
   private Convocatoria  convocatoria;

    public Convocatoria getConvocatoria() {
        return convocatoria;
    }

    public void setConvocatoria(Convocatoria convocatoria) {
        this.convocatoria = convocatoria;
    }
   
    public Date getCreado() {
        return creado;
    }

    public void setCreado(Date creado) {
        this.creado = creado;
    }

    public Date getActualizado() {
        return actualizado;
    }

    public int getHorasRealizar() {
        return horasRealizar;
    }

    public void setHorasRealizar(int horasRealizar) {
        this.horasRealizar = horasRealizar;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getCiclo() {
        return ciclo;
    }

    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }

    public String getParalelo() {
        return paralelo;
    }

    public void setParalelo(String paralelo) {
        this.paralelo = paralelo;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public void setActualizado(Date actualizado) {
        this.actualizado = actualizado;
    }

    public Long getIdsolicitudEstudiante() {
        return idsolicitudEstudiante;
    }

    public void setIdsolicitudEstudiante(Long idsolicitudEstudiante) {
        this.idsolicitudEstudiante = idsolicitudEstudiante;
    }

    public int getNumeroEstudiantes() {
        return numeroEstudiantes;
    }

    public void setNumeroEstudiantes(int numeroEstudiantes) {
        this.numeroEstudiantes = numeroEstudiantes;
    }
 
    public Date getFechaInterna() {
        return fechaInterna;
    }

    public void setFechaInterna(Date fechaInterna) {
        this.fechaInterna = fechaInterna;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }



    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

}
