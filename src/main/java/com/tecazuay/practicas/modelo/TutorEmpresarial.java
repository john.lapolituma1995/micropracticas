
package com.tecazuay.practicas.modelo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author HP
 */
@Entity
@Audited
@JsonIgnoreProperties(value = {"hibernateLazyInitializer"})
@EntityListeners({ AuditingEntityListener.class })
public class TutorEmpresarial implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idtutorEmpresarial;
    private String cedula;
    private String nombre;
    private String apellido;
    private String telefono;
    private String correo;
    private String cargo;
    //listo
     //relacionempresa(listo)soy goku hollaaaaaaaaaaaaaaa
     //relacionempresa(listo)soy goku hollaaaaaaaaaaaaaaa
 @CreatedDate
    Date creado;
    @LastModifiedDate
    Date actualizado;
    @ManyToOne
    private Empresa empresas;

    public Empresa getEmpresas() {
        return empresas;
    }

    public void setEmpresas(Empresa empresas) {
        this.empresas = empresas;
    }

    public Long getIdtutorEmpresarial() {
        return idtutorEmpresarial;
    }

    public void setIdtutorEmpresarial(Long idtutorEmpresarial) {
        this.idtutorEmpresarial = idtutorEmpresarial;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

}
