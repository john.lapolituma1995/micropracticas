
package com.tecazuay.practicas.modelo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;



@Entity
@Audited
@JsonIgnoreProperties(value = {"hibernateLazyInitializer"})
@EntityListeners({ AuditingEntityListener.class })
public class InformeTutorAcademico implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idInforme;
    private Date fechaExterna;
    private float notaAcademico;
    private float notaEmpresarial;
     private float notapromAcademico;
    private float notapromEmpresarial;
     private float puntajeTotal;
     private int horasTotal;
    private String periodo;
     private String carrera;
    private String estado;
    private String responsableppp;
    //servicioCarrera
     //servicioEstudiante
    //servicicioCiclo
    //servivioTutorAcademico(nombre )
    //relacionRespuestaEstudiante
    
    @CreatedDate
    Date creado;
    @LastModifiedDate
    Date actualizado;

    public Date getCreado() {
        return creado;
    }

    public void setCreado(Date creado) {
        this.creado = creado;
    }

    public Date getActualizado() {
        return actualizado;
    }

    public void setActualizado(Date actualizado) {
        this.actualizado = actualizado;
    }

    public int getHorasTotal() {
        return horasTotal;
    }

    public void setHorasTotal(int horasTotal) {
        this.horasTotal = horasTotal;
    }
    
    
    public Long getIdPerfil() {
        return idInforme;
    }

    public void setIdPerfil(Long idInforme) {
        this.idInforme = idInforme;
    }

    public Long getIdInforme() {
        return idInforme;
    }

    public String getCarrera() {
        return carrera;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getResponsableppp() {
        return responsableppp;
    }

    public void setResponsableppp(String responsableppp) {
        this.responsableppp = responsableppp;
    }

    public void setIdInforme(Long idInforme) {
        this.idInforme = idInforme;
    }

    public Date getFechaExterna() {
        return fechaExterna;
    }

    public void setFechaExterna(Date fechaExterna) {
        this.fechaExterna = fechaExterna;
    }

    public float getNotaAcademico() {
        return notaAcademico;
    }

    public void setNotaAcademico(float notaAcademico) {
        this.notaAcademico = notaAcademico;
    }

    public float getNotaEmpresarial() {
        return notaEmpresarial;
    }

    public void setNotaEmpresarial(float notaEmpresarial) {
        this.notaEmpresarial = notaEmpresarial;
    }

    public float getNotapromAcademico() {
        return notapromAcademico;
    }

    public void setNotapromAcademico(float notapromAcademico) {
        this.notapromAcademico = notapromAcademico;
    }

    public float getNotapromEmpresarial() {
        return notapromEmpresarial;
    }

    public void setNotapromEmpresarial(float notapromEmpresarial) {
        this.notapromEmpresarial = notapromEmpresarial;
    }

    public float getPuntajeTotal() {
        return puntajeTotal;
    }

    public void setPuntajeTotal(float puntajeTotal) {
        this.puntajeTotal = puntajeTotal;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }



  
}
