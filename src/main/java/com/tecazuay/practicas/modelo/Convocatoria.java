package com.tecazuay.practicas.modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author HP
 */
@Entity
@Audited
@EntityListeners({ AuditingEntityListener.class })
public class Convocatoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idConvocatoria;
    private String codConvocatoria;
    private String estado;
     private Date fecha;
    private Date fechaInterna;
    private Date fechaMaximo;
    private String  responsableppp;
    private String ciclo;
    
    //relacionsolicitud
    /* @ManyToOne
    private SolicitudEmpresa solicitudempresas;

    public SolicitudEmpresa getSolicitudempresas() {
        return solicitudempresas;
    }

    public void setSolicitudempresas(SolicitudEmpresa solicitudempresas) {
        this.solicitudempresas = solicitudempresas;
    }
     */
      @CreatedDate
    Date creado;
    @LastModifiedDate
    Date actualizado;

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getCreado() {
        return creado;
    }

    public void setCreado(Date creado) {
        this.creado = creado;
    }

    public Date getActualizado() {
        return actualizado;
    }

    public void setActualizado(Date actualizado) {
        this.actualizado = actualizado;
    }
    
    @OneToOne
    private SolicitudEmpresa solicitudempresas;
    

    @ManyToOne
    private SolicitudEstudiante solicitudestudiante;

    public SolicitudEstudiante getSolicitudestudiante() {
        return solicitudestudiante;
    }

    public void setSolicitudestudiante(SolicitudEstudiante solicitudestudiante) {
        this.solicitudestudiante = solicitudestudiante;
    }

    public SolicitudEmpresa getSolicitudempresas() {
        return solicitudempresas;
    }

    public void setSolicitudempresas(SolicitudEmpresa solicitudempresas) {
        this.solicitudempresas = solicitudempresas;
    }

    public Long getIdConvocatoria() {
        return idConvocatoria;
    }

    public void setIdConvocatoria(Long idConvocatoria) {
        this.idConvocatoria = idConvocatoria;
    }

    public String getCodConvocatoria() {
        return codConvocatoria;
    }

    public void setCodConvocatoria(String codConvocatoria) {
        this.codConvocatoria = codConvocatoria;
    }


    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaInterna() {
        return fechaInterna;
    }

    public void setFechaInterna(Date fechaInterna) {
        this.fechaInterna = fechaInterna;
    }

    public String getResponsableppp() {
        return responsableppp;
    }

    public void setResponsableppp(String responsableppp) {
        this.responsableppp = responsableppp;
    }

 
 

    public String getCiclo() {
        return ciclo;
    }

    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }

    public Date getFechaMaximo() {
        return fechaMaximo;
    }

    public void setFechaMaximo(Date fechaMaximo) {
        this.fechaMaximo = fechaMaximo;
    }

}
