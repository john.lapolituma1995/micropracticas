package com.tecazuay.practicas.modelo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Audited
@JsonIgnoreProperties(value = {"hibernateLazyInitializer"})
@EntityListeners({ AuditingEntityListener.class })
public class Empresa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private Long idEmpresa;
    private String ruc;
    private String nombre;
    private String descripcion;
    private String direccion;
    private String telefono;
    private String correo;
  private String representante;
  private String tipo;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getRepresentante() {
        return representante;
    }

    public void setRepresentante(String representante) {
        this.representante = representante;
    }
    
    
      @CreatedDate
    Date creado;
    @LastModifiedDate
    Date actualizado;
    

    public Date getCreado() {
        return creado;
    }



    public void setCreado(Date creado) {
        this.creado = creado;
    }

    public Date getActualizado() {
        return actualizado;
    }



    public void setActualizado(Date actualizado) {
        this.actualizado = actualizado;
    }
    

    //@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    //private List<TutorEmpresarial> tutores;
//    
//     @OneToMany
//     List<TutorEmpresarial> tutores;
//
//
//
//    public List<TutorEmpresarial> getTutores() {
//        return tutores;
//    }
//
//    public void setTutores(List<TutorEmpresarial> tutores) {
//        this.tutores = tutores;
//    }

     
    public Long getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Long idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

}
