
package com.tecazuay.practicas.modelo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;



@Entity
@Audited
@JsonIgnoreProperties(value = {"hibernateLazyInitializer"})
@EntityListeners({ AuditingEntityListener.class })
public class Reunion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idReunion;
    private Date  fechaExterna;
    private String lugar;
     private String estado;
    private String observaciones;
    private int horasPracticas;
     private Date  fechaInicio;
     private Date  fechaFin;
     private Date horaInicio;
     private Date horaFin;
     
    //servicioresponsablePPP
    //serviciocarrera
    //servicoestudiante
    //servicio ciclo
    //relacion con empresa o tutorempresa
    //relacion solicitudempresa
     
    @CreatedDate
    Date creado;
    @LastModifiedDate
    Date actualizado;
 @ManyToOne
    private SolicitudEmpresa  solicitudempresa;

    public SolicitudEmpresa getSolicitudempresa() {
        return solicitudempresa;
    }

    public void setSolicitudempresa(SolicitudEmpresa solicitudempresa) {
        this.solicitudempresa = solicitudempresa;
    }
 
    public Date getCreado() {
        return creado;
    }

    public void setCreado(Date creado) {
        this.creado = creado;
    }

    public Date getActualizado() {
        return actualizado;
    }

    public void setActualizado(Date actualizado) {
        this.actualizado = actualizado;
    }

    public void setIdReunion(Long idReunion) {
        this.idReunion = idReunion;
    }

    public Date getFechaExterna() {
        return fechaExterna;
    }

    public void setFechaExterna(Date fechaExterna) {
        this.fechaExterna = fechaExterna;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public int getHorasPracticas() {
        return horasPracticas;
    }

    public void setHorasPracticas(int horasPracticas) {
        this.horasPracticas = horasPracticas;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(Date horaInicio) {
        this.horaInicio = horaInicio;
    }


    

    public Date getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(Date horaFin) {
        this.horaFin = horaFin;
    }

  
    

  
}
