
package com.tecazuay.practicas.modelo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;



@Entity
@Audited
@JsonIgnoreProperties(value = {"hibernateLazyInitializer"})
@EntityListeners({ AuditingEntityListener.class })
public class CertificadoEmpresa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idCertificado;
    private Date fechaExterna;
   private String estado;
    private Date fechaInicio;
    private Date fechaFin;
    private String actividades;
    private int horasTotal;
    private float calificacion;
     private String responsableppp;
         private String  carrera;
    
    //servicioResponsabelPPP
    //relacion SolicitudEmpresa(tutorEmpresa,cedula,nombre)
    //servicioEstudiante 
    //servicioCarrera
    @CreatedDate
    Date creado;
    @LastModifiedDate
    Date actualizado;
    
    
    @ManyToOne
    private TutorEmpresarial  tutorempresarial;

    public TutorEmpresarial getTutorempresarial() {
        return tutorempresarial;
    }

    public void setTutorempresarial(TutorEmpresarial tutorempresarial) {
        this.tutorempresarial = tutorempresarial;
    }

  

        
    
    public Date getCreado() {
        return creado;
    }

    public void setCreado(Date creado) {
        this.creado = creado;
    }

    public Date getActualizado() {
        return actualizado;
    }

    public void setActualizado(Date actualizado) {
        this.actualizado = actualizado;
    }
    
    
    public Long getIdPerfil() {
        return idCertificado;
    }

    public void setIdPerfil(Long idCertificado) {
        this.idCertificado = idCertificado;
    }

    public String getResponsableppp() {
        return responsableppp;
    }

    public void setResponsableppp(String responsableppp) {
        this.responsableppp = responsableppp;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }
    

    public Long getIdCertificado() {
        return idCertificado;
    }

    public void setIdCertificado(Long idCertificado) {
        this.idCertificado = idCertificado;
    }

    public Date getFechaExterna() {
        return fechaExterna;
    }

    public void setFechaExterna(Date fechaExterna) {
        this.fechaExterna = fechaExterna;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getActividades() {
        return actividades;
    }

    public void setActividades(String actividades) {
        this.actividades = actividades;
    }

    public int getHorasTotal() {
        return horasTotal;
    }

    public void setHorasTotal(int horasTotal) {
        this.horasTotal = horasTotal;
    }

    public float getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(float calificacion) {
        this.calificacion = calificacion;
    }

  
}
