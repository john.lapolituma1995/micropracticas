package com.tecazuay.practicas.modelo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import org.hibernate.envers.Audited;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author HP
 */
@Entity
@Audited
@EntityListeners({AuditingEntityListener.class})
@JsonIgnoreProperties(value = {"hibernateLazyInitializer"})
public class SolicitudEmpresa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idSolicitudEmpresa;
    private String responsableppp;
    //private Date fechaInterna;
    private Date fechaExterna;
    private String estado;
    //private String periodo;
    private int numeroEstudiantes;
    private String actividades;
    private Date fechaInicio;
  
    private String  carrera;
     private String cargo;

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
     

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getResponsableppp() {
        return responsableppp;
    }

    public void setResponsableppp(String responsableppp) {
        this.responsableppp = responsableppp;
    }
    
    public String getActividades() {
        return actividades;
    }

    public void setActividades(String actividades) {
        this.actividades = actividades;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }
    
   
  @CreatedDate
    Date creado;
    @LastModifiedDate
    Date actualizado;
    //relacion docente encargadoPPP,empresa listo,carrera,tutorEmpresarial list0
   
    //reviar relacion 
//    @ManyToOne
//    private TutorEmpresarial tutorempresa;

    @ManyToOne
    private Empresa empresa;

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

//    public TutorEmpresarial getTutorempresa() {
//        return tutorempresa;
//    }
//
//    public void setTutorempresa(TutorEmpresarial tutorempresa) {
//        this.tutorempresa = tutorempresa;
//    }

    public Long getIdSolicitudEmpresa() {
        return idSolicitudEmpresa;
    }

    public void setIdSolicitudEmpresa(Long idSolicitudEmpresa) {
        this.idSolicitudEmpresa = idSolicitudEmpresa;
    }

//    public Date getFechaInterna() {
//        return fechaInterna;
//    }
//
//    public void setFechaInterna(Date fechaInterna) {
//        this.fechaInterna = fechaInterna;
//    }

    public Date getFechaExterna() {
        return fechaExterna;
    }

    public void setFechaExterna(Date fechaExterna) {
        this.fechaExterna = fechaExterna;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

//    public String getPeriodo() {
//        return periodo;
//    }
//
//    public void setPeriodo(String periodo) {
//        this.periodo = periodo;
//    }
      public Date getCreado() {
        return creado;
    }

    public void setCreado(Date creado) {
        this.creado = creado;
    }

    public Date getActualizado() {
        return actualizado;
    }

    public void setActualizado(Date actualizado) {
        this.actualizado = actualizado;
    }
    
    
    public int getNumeroEstudiantes() {
        return numeroEstudiantes;
    }

    public void setNumeroEstudiantes(int numeroEstudiantes) {
        this.numeroEstudiantes = numeroEstudiantes;
    }
}
