package com.tecazuay.practicas.seguridad;

import com.google.gson.Gson;

import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class JwtTokenUtil implements Serializable {

    private static final long serialVersionUID = 6954121380881122063L;
    static final String CLAIM_KEY_USERNAME = "sub";
    static final String CLAIM_KEY_CREATED = "created";

    private String secret = "ddwewcxxc";

    private Long expiration = 1800L;

    public void setSecret(String sec) {
        this.secret = sec;
    }

    public UsuarioToken getUserFromToken(String token) {
        Gson gson = new Gson();
        UsuarioToken usuario = null;
        ObjectMapper mapper = new ObjectMapper();
        Object algo = null;
        String otroAlgo;
        try {
            final Claims claims = getClaimsFromToken(token);
            algo = claims.get("usuarioData");
            otroAlgo = gson.toJson(algo);
            usuario = mapper.readValue(otroAlgo, UsuarioToken.class);
        } catch (Exception e) {
            e.printStackTrace();
            usuario = null;
        }
        return usuario;
    }

//    public String getCedulaFromToken(String token) {
//        Gson gson = new Gson();
//        String cedula = null;
//        ObjectMapper mapper = new ObjectMapper();
//        Object algo = null;
//        String otroAlgo;
//        try {
//            final Claims claims = getClaimsFromToken(token);
//            algo = claims.get("cedula");
//            otroAlgo = gson.toJson(algo);
//            cedula = mapper.readValue(otroAlgo, String.class);
//        } catch (Exception e) {
//            e.printStackTrace();
//            cedula = null;
//        }
//        return cedula;
//    }

    public Date getCreatedDateFromToken(String token) {
        Date created;
        try {
            final Claims claims = getClaimsFromToken(token);
            created = new Date((Long) claims.get(CLAIM_KEY_CREATED));
        } catch (Exception e) {
            created = null;
        }
        return created;
    }

    public Date getExpirationDateFromToken(String token) {
        Date expiration;
        try {
            final Claims claims = getClaimsFromToken(token);
            expiration = claims.getExpiration();
        } catch (Exception e) {
            expiration = null;
        }
        return expiration;
    }

    private Claims getClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser().setSigningKey(secret).setAllowedClockSkewSeconds(10).parseClaimsJws(token).getBody();
        } catch (Exception e) {
            claims = null;
            // e.printStackTrace();
        }
        return claims;
    }

    private Date generateExpirationDate() {
        return new Date(System.currentTimeMillis() + expiration * 1000);
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        if (expiration == null) {
            return true;
        }
        return expiration.before(new Date());
    }

    // public String generateToken(String username) {
    public String generateToken(UsuarioToken usuario) {
        Map<String, Object> claims = new HashMap<>();
        // claims.put(CLAIM_KEY_USERNAME, username);
        claims.put(CLAIM_KEY_CREATED, new Date());
        /*
         * Usuario usuario = new Usuario(); usuario.setUsername("username asdf");
         * Permiso permiso = new Permiso(); permiso.setNombre("per");
         * usuario.getPermisos().add(permiso);
         */
        claims.put("usuario", usuario);
        // claims.put("cedula", cedula);
        return generateToken(claims);
    }

    private String generateToken(Map<String, Object> claims) {
        return Jwts.builder().setClaims(claims).setExpiration(generateExpirationDate())
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }

    public Boolean canTokenBeRefreshed(String token) {
        return (!isTokenExpired(token));
    }

    public String refreshToken(String token) {
        if (!canTokenBeRefreshed(token)) {
            return null;
        }
        String refreshedToken;
        try {
            final Claims claims = getClaimsFromToken(token);
            claims.put(CLAIM_KEY_CREATED, new Date());
            refreshedToken = generateToken(claims);
        } catch (Exception e) {
            refreshedToken = null;
        }
        return refreshedToken;
    }

    public Boolean validateToken(String token) {
        return !isTokenExpired(token);
    }
    
//	public Optional findUserByResetToken(String resetToken) {
//		return userRepository.findByResetToken(resetToken);
//	}
}
