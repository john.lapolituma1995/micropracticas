package com.tecazuay.practicas.seguridad;

public class Permiso {

    private String nombre;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Permiso(String p) {
        this.nombre = p;
    }

    public Permiso() {
    }
}
