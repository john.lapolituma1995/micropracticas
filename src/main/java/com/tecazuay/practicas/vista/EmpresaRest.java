
package com.tecazuay.practicas.vista;

import com.tecazuay.practicas.controlador.EmpresaRepositorio;
import com.tecazuay.practicas.modelo.Empresa;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author casa
 */
@RestController
@RequestMapping("/empresa")
public class EmpresaRest {

    @Autowired
   EmpresaRepositorio empresaRepositorio;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public List<Empresa> listar() {
        return empresaRepositorio.findAll();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin
    public Empresa guardar(@RequestBody Empresa p) {
        return empresaRepositorio.save(p);
    }
    @RequestMapping(value = "/{idEmpresa}", method = RequestMethod.GET)
    @ResponseBody
    public Empresa leer(@PathVariable Long idEmpresa) {
        return empresaRepositorio.getOne(idEmpresa);
    }
    @RequestMapping(value = "/{idEmpresa}", method = RequestMethod.DELETE)
    @ResponseBody
    @CrossOrigin
    public void borrar(@PathVariable Long idEmpresa) {
        empresaRepositorio.deleteById(idEmpresa);
    }
}
