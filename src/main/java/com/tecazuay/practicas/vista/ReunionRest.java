package com.tecazuay.practicas.vista;

import com.tecazuay.practicas.controlador.ReunionRepositorio;
import com.tecazuay.practicas.controlador.ReunionRepositorio;
import com.tecazuay.practicas.modelo.Reunion;
import com.tecazuay.practicas.modelo.Reunion;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/reunion")
public class ReunionRest {
   @Autowired
   ReunionRepositorio reunionRepositorio;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public List<Reunion> listar() {
        return reunionRepositorio.findAll();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin
    public Reunion guardar(@RequestBody Reunion p) {
        return reunionRepositorio.save(p);
    }
    @RequestMapping(value = "/{idReunion}", method = RequestMethod.GET)
    @ResponseBody
    public Reunion leer(@PathVariable Long idReunion) {
        return reunionRepositorio.getOne(idReunion);
    }
    @RequestMapping(value = "/{idReunion}", method = RequestMethod.DELETE)
    @ResponseBody
    @CrossOrigin
    public void borrar(@PathVariable Long idReunion) {
        reunionRepositorio.deleteById(idReunion);
    }
}
