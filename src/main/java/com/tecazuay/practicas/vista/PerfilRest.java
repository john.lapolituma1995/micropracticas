package com.tecazuay.practicas.vista;

import com.tecazuay.practicas.controlador.PerfilRepositorio;
import com.tecazuay.practicas.controlador.PerfilRepositorio;
import com.tecazuay.practicas.modelo.Perfil;
import com.tecazuay.practicas.modelo.Perfil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/perfil")
public class PerfilRest {
   @Autowired
   PerfilRepositorio perfilRepositorio;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public List<Perfil> listar() {
        return perfilRepositorio.findAll();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin
    public Perfil guardar(@RequestBody Perfil p) {
        return perfilRepositorio.save(p);
    }
    @RequestMapping(value = "/{idPerfil}", method = RequestMethod.GET)
    @ResponseBody
    public Perfil leer(@PathVariable Long idPerfil) {
        return perfilRepositorio.getOne(idPerfil);
    }
    @RequestMapping(value = "/{idPerfil}", method = RequestMethod.DELETE)
    @ResponseBody
    @CrossOrigin
    public void borrar(@PathVariable Long idPerfil) {
        perfilRepositorio.deleteById(idPerfil);
    }
}
