package com.tecazuay.practicas.vista;


import com.tecazuay.practicas.controlador.RespuestaEstudianteRepositorio;

import com.tecazuay.practicas.modelo.RespuestaEstudiante;
import io.swagger.annotations.ApiImplicitParam;

import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/respuestaestudiante")
public class RespuestaEstudianteRest {
   @Autowired
   RespuestaEstudianteRepositorio respuestaRepositorio;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public List<RespuestaEstudiante> listar() {
        return respuestaRepositorio.findAll();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin
    public RespuestaEstudiante guardar(@RequestBody RespuestaEstudiante p) {
        return respuestaRepositorio.save(p);
    }
    @RequestMapping(value = "/{idRespuesta}", method = RequestMethod.GET)
    @ResponseBody
    public RespuestaEstudiante leer(@PathVariable Long idRespuesta) {
        return respuestaRepositorio.getOne(idRespuesta);
    }
    @RequestMapping(value = "/{idRespuesta}", method = RequestMethod.DELETE)
    @ResponseBody
    @CrossOrigin
    public void borrar(@PathVariable Long idRespuesta) {
        respuestaRepositorio.deleteById(idRespuesta);
    }
}
