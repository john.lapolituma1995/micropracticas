
package com.tecazuay.practicas.vista;

import com.tecazuay.practicas.controlador.ConvocatoriaRepositorio;
import com.tecazuay.practicas.modelo.Convocatoria;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author casa
 */
@RestController
@RequestMapping("/convocatoria")
public class ConvocatoriaRest {

    @Autowired
   ConvocatoriaRepositorio convocatoriaRepositorio;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public List<Convocatoria> listar() {
        return convocatoriaRepositorio.findAll();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin
    public Convocatoria guardar(@RequestBody Convocatoria p) {
        return convocatoriaRepositorio.save(p);
    }
    @RequestMapping(value = "/{idConvocatoria}", method = RequestMethod.GET)
    @ResponseBody
    public Convocatoria leer(@PathVariable Long idConvocatoria) {
        return convocatoriaRepositorio.getOne(idConvocatoria);
    }
    @RequestMapping(value = "/{idConvocatoria}", method = RequestMethod.DELETE)
    @ResponseBody
    @CrossOrigin
    public void borrar(@PathVariable Long idConvocatoria) {
        convocatoriaRepositorio.deleteById(idConvocatoria);
    }
}
