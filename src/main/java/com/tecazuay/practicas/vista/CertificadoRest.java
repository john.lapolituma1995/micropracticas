package com.tecazuay.practicas.vista;

import com.tecazuay.practicas.controlador.CertificadoRepositorio;
import com.tecazuay.practicas.controlador.CertificadoRepositorio;
import com.tecazuay.practicas.modelo.CertificadoEmpresa;
import com.tecazuay.practicas.modelo.CertificadoEmpresa;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/certificadoempresa")
public class CertificadoRest {
   @Autowired
   CertificadoRepositorio certificadoRepositorio;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public List<CertificadoEmpresa> listar() {
        return certificadoRepositorio.findAll();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin
    public CertificadoEmpresa guardar(@RequestBody CertificadoEmpresa p) {
        return certificadoRepositorio.save(p);
    }
    @RequestMapping(value = "/{idCertificado}", method = RequestMethod.GET)
    @ResponseBody
    public CertificadoEmpresa leer(@PathVariable Long idCertificado) {
        return certificadoRepositorio.getOne(idCertificado);
    }
    @RequestMapping(value = "/{idCertificado}", method = RequestMethod.DELETE)
    @ResponseBody
    @CrossOrigin
    public void borrar(@PathVariable Long idCertificado) {
        certificadoRepositorio.deleteById(idCertificado);
    }
}
