
package com.tecazuay.practicas.vista;

import com.tecazuay.practicas.controlador.TutorEmpresarialRepositorio;
import com.tecazuay.practicas.modelo.TutorEmpresarial;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author casa
 */
@RestController
@RequestMapping("/tutorempresarial")
public class TutorEmpresarialRest {

    @Autowired
   TutorEmpresarialRepositorio tutorempresarialRepositorio;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public List<TutorEmpresarial> listar() {
        return tutorempresarialRepositorio.findAll();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin
    public TutorEmpresarial guardar(@RequestBody TutorEmpresarial p) {
        return tutorempresarialRepositorio.save(p);
    }
    @RequestMapping(value = "/{idtutorEmpresarial}", method = RequestMethod.GET)
    @ResponseBody
    public TutorEmpresarial leer(@PathVariable Long idtutorEmpresarial) {
        return tutorempresarialRepositorio.getOne(idtutorEmpresarial);
    }
    @RequestMapping(value = "/{idtutorEmpresarial}", method = RequestMethod.DELETE)
    @ResponseBody
    @CrossOrigin
    public void borrar(@PathVariable Long idtutorEmpresarial) {
        tutorempresarialRepositorio.deleteById(idtutorEmpresarial);
    }
}
