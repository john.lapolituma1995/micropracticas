
package com.tecazuay.practicas.vista;

import com.tecazuay.practicas.controlador.SolicitudEmpresaRepositorio;
import com.tecazuay.practicas.modelo.SolicitudEmpresa;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author casa
 */
@RestController
@RequestMapping("/solicitudempresa")
public class SolicitudEmpresaRest {

    @Autowired
  SolicitudEmpresaRepositorio solicitudempresaRepositorio;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public List<SolicitudEmpresa> listar() {
        return solicitudempresaRepositorio.findAll();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin
    public SolicitudEmpresa guardar(@RequestBody SolicitudEmpresa p) {
        return solicitudempresaRepositorio.save(p);
    }
    @RequestMapping(value = "/{idSolicitudEmpresa}", method = RequestMethod.GET)
    @ResponseBody
    public SolicitudEmpresa leer(@PathVariable Long idSolicitudEmpresa) {
        return solicitudempresaRepositorio.getOne(idSolicitudEmpresa);
    }
//      @RequestMapping(value = "/{idSolicitudEmpresa}", method = RequestMethod.GET)
//    @ResponseBody
//    public SolicitudEmpresa hola(@PathVariable Long idSolicitudEmpresa) {
//        return solicitudempresaRepositorio.getOne(idSolicitudEmpresa);
//    }
    @RequestMapping(value = "/{idSolicitudEmpresa}", method = RequestMethod.DELETE)
    @ResponseBody
    @CrossOrigin
    public void borrar(@PathVariable Long idSolicitudEmpresa) {
        solicitudempresaRepositorio.deleteById(idSolicitudEmpresa);
    }
}
