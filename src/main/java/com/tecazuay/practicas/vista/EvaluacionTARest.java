package com.tecazuay.practicas.vista;


import com.tecazuay.practicas.controlador.EvaluacionTARepositorio;

import com.tecazuay.practicas.modelo.EvaluacionTutorAcademico;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/evaluaciontutoracademico")
public class EvaluacionTARest {
   @Autowired
   EvaluacionTARepositorio evaluacionRepositorio;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public List<EvaluacionTutorAcademico> listar() {
        return evaluacionRepositorio.findAll();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin
    public EvaluacionTutorAcademico guardar(@RequestBody EvaluacionTutorAcademico p) {
        return evaluacionRepositorio.save(p);
    }
    
    @RequestMapping(value = "/{idEvaluacion}", method = RequestMethod.GET)
    @ResponseBody
    public EvaluacionTutorAcademico leer(@PathVariable Long idEvaluacion) {
        return evaluacionRepositorio.getOne(idEvaluacion);
    }
    @RequestMapping(value = "/{idEvaluacion}", method = RequestMethod.DELETE)
    @ResponseBody
    @CrossOrigin
    public void borrar(@PathVariable Long idEvaluacion) {
        evaluacionRepositorio.deleteById(idEvaluacion);
    }
}
