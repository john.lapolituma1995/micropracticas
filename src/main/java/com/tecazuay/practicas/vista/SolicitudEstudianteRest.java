
package com.tecazuay.practicas.vista;

import com.tecazuay.practicas.controlador.SolicitudEstudianteRepositorio;
import com.tecazuay.practicas.modelo.SolicitudEstudiante;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author casa
 */
@RestController
@RequestMapping("/solicitudestudiante")
public class SolicitudEstudianteRest {

    @Autowired
   SolicitudEstudianteRepositorio solicitudestudianteRepositorio;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public List<SolicitudEstudiante> listar() {
        return solicitudestudianteRepositorio.findAll();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin
    public SolicitudEstudiante guardar(@RequestBody SolicitudEstudiante p) {
        return solicitudestudianteRepositorio.save(p);
    }
    @RequestMapping(value = "/{idSolicitudEstudiante}", method = RequestMethod.GET)
    @ResponseBody
    public SolicitudEstudiante leer(@PathVariable Long idSolicitudEstudiante) {
        return solicitudestudianteRepositorio.getOne(idSolicitudEstudiante);
    }
    @RequestMapping(value = "/{idSolicitudEstudiante}", method = RequestMethod.DELETE)
    @ResponseBody
    @CrossOrigin
    public void borrar(@PathVariable Long idSolicitudEstudiante) {
        solicitudestudianteRepositorio.deleteById(idSolicitudEstudiante);
    }
}
