
package com.tecazuay.practicas.vista;

import com.google.gson.Gson;
import com.tecazuay.practicas.controlador.UsuarioRepositorio;
import com.tecazuay.practicas.modelo.Usuario;
import com.tecazuay.practicas.modelo.dto.AuthDto;
import com.tecazuay.practicas.modelo.dto.Respuesta;
import com.tecazuay.practicas.modelo.dto.UsuarioDto;
import com.tecazuay.practicas.seguridad.JwtTokenUtil;
import com.tecazuay.practicas.seguridad.UsuarioToken;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author casa
 */
@RestController
@RequestMapping("/usuario")
public class UsuarioRest {

    @Autowired
   UsuarioRepositorio usuarioRepositorio;
     @Autowired
      private PasswordEncoder passwordEncoder;
    @Autowired
    private JwtTokenUtil jwtutil;
    

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public List<Usuario> listar() {
        return usuarioRepositorio.findAll();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin
    public Usuario guardar(@RequestBody Usuario p) {
            try {
            p.setContrasena(this.passwordEncoder.encode(p.getContrasena()));
            return usuarioRepositorio.save(p);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
      //  return usuarioRepositorio.save(p);
    }
    @RequestMapping(value = "/{idUsuario}", method = RequestMethod.GET)
    @ResponseBody
    public Usuario leer(@PathVariable Long idUsuario) {
        return usuarioRepositorio.getOne(idUsuario);
    }
    @RequestMapping(value = "/{idUsuario}", method = RequestMethod.DELETE)
    @ResponseBody
    @CrossOrigin
    public void borrar(@PathVariable Long idUsuario) {
        usuarioRepositorio.deleteById(idUsuario);
    }
    
        @RequestMapping(value = "/logueo", method = RequestMethod.POST)
        public Respuesta logueo(@RequestBody AuthDto usuario)       {
        try {

            Usuario u = usuarioRepositorio.validarUsuario(usuario.getUsuario());
            Respuesta r = new Respuesta();
            Gson gson = new Gson();
            if (u!=null && passwordEncoder.matches(usuario.getPass(), u.getContrasena())) {
                r.setUsuario(gson.fromJson(gson.toJson(u), UsuarioDto.class));
                UsuarioToken ut = new UsuarioToken();
                ut.setUsername(u.getUsuario());
                //ut.setIdentificacion(u.getUsuario());
               
                r.setValido(true);
                r.setToken(jwtutil.generateToken(ut));
                
            } else {
                r.setValido(false);
            }

            return r;
        } catch (Exception e) {
            e.printStackTrace();

            return null;

        }
    }

//    @RequestMapping(value = "/{recordarContrasena}", method = RequestMethod.POST)
//    @ResponseBody
//    @CrossOrigin
//   public Respuesta  resetPassword(HttpServletRequest request, 
//  @RequestParam("email") String userEmail) {
//    User user = userService.findUserByEmail(userEmail);
//    if (user == null) {
//        throw new UserNotFoundException();
//    }
//    String token = UUID.randomUUID().toString();
//    userService.createPasswordResetTokenForUser(user, token);
//    mailSender.send(constructResetTokenEmail(getAppUrl(request), 
//      request.getLocale(), token, user));
//    return new GenericResponse(
//      messages.getMessage("message.resetPasswordEmail", null, 
//      request.getLocale()));
//}
}

