package com.tecazuay.practicas.vista;

import com.tecazuay.practicas.controlador.PlanificaciontutorARepositorio;
import com.tecazuay.practicas.controlador.PlanificaciontutorARepositorio;
import com.tecazuay.practicas.modelo.PlanificacionTutorAcademico;
import com.tecazuay.practicas.modelo.PlanificacionTutorAcademico;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/planificaciontutoracademico")
public class PlanificaciontutorARest {
   @Autowired
   PlanificaciontutorARepositorio planificacionRepositorio;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public List<PlanificacionTutorAcademico> listar() {
        return planificacionRepositorio.findAll();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin
    public PlanificacionTutorAcademico guardar(@RequestBody PlanificacionTutorAcademico p) {
        return planificacionRepositorio.save(p);
    }
    @RequestMapping(value = "/{idPlanificacion}", method = RequestMethod.GET)
    @ResponseBody
    public PlanificacionTutorAcademico leer(@PathVariable Long idPlanificacion) {
        return planificacionRepositorio.getOne(idPlanificacion);
    }
    @RequestMapping(value = "/{idPlanificacion}", method = RequestMethod.DELETE)
    @ResponseBody
    @CrossOrigin
    public void borrar(@PathVariable Long idPlanificacion) {
        planificacionRepositorio.deleteById(idPlanificacion);
    }
}
