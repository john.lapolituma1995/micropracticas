package com.tecazuay.practicas.vista;


import com.tecazuay.practicas.controlador.RespuestaEmpresaRepositorio;

import com.tecazuay.practicas.modelo.RespuestaEmpresa;
import io.swagger.annotations.ApiImplicitParam;

import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/respuestaempresa")
public class RespuestaEmpresaRest {
   @Autowired
   RespuestaEmpresaRepositorio respuestaEmpresaRepositorio;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public List<RespuestaEmpresa> listar() {
        return respuestaEmpresaRepositorio.findAll();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin
    public RespuestaEmpresa guardar(@RequestBody RespuestaEmpresa p) {
        return respuestaEmpresaRepositorio.save(p);
    }
    @RequestMapping(value = "/{idRespuesta}", method = RequestMethod.GET)
    @ResponseBody
    public RespuestaEmpresa leer(@PathVariable Long idRespuesta) {
        return respuestaEmpresaRepositorio.getOne(idRespuesta);
    }
    @RequestMapping(value = "/{idRespuesta}", method = RequestMethod.DELETE)
    @ResponseBody
    @CrossOrigin
    public void borrar(@PathVariable Long idRespuesta) {
        respuestaEmpresaRepositorio.deleteById(idRespuesta);
    }
}
