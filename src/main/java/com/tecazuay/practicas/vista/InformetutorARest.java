package com.tecazuay.practicas.vista;

import com.tecazuay.practicas.controlador.InformetutorARepositorio;
import com.tecazuay.practicas.controlador.InformetutorARepositorio;
import com.tecazuay.practicas.modelo.InformeTutorAcademico;
import com.tecazuay.practicas.modelo.InformeTutorAcademico;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/informetutoracademico")
public class InformetutorARest {
   @Autowired
   InformetutorARepositorio informeRepositorio;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public List<InformeTutorAcademico> listar() {
        return informeRepositorio.findAll();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin
    public InformeTutorAcademico guardar(@RequestBody InformeTutorAcademico p) {
        return informeRepositorio.save(p);
    }
    @RequestMapping(value = "/{idInforme}", method = RequestMethod.GET)
    @ResponseBody
    public InformeTutorAcademico leer(@PathVariable Long idInforme) {
        return informeRepositorio.getOne(idInforme);
    }
    @RequestMapping(value = "/{idInforme}", method = RequestMethod.DELETE)
    @ResponseBody
    @CrossOrigin
    public void borrar(@PathVariable Long idInforme) {
        informeRepositorio.deleteById(idInforme);
    }
}
