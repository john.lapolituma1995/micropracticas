
package com.tecazuay.practicas.controlador;


import com.tecazuay.practicas.modelo.Convocatoria;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author casa
 */
public interface ConvocatoriaRepositorio extends JpaRepository<Convocatoria, Long>{
    
}
