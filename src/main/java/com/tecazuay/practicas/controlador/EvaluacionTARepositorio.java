
package com.tecazuay.practicas.controlador;


import com.tecazuay.practicas.modelo.EvaluacionTutorAcademico;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author casa
 */
public interface EvaluacionTARepositorio extends JpaRepository<EvaluacionTutorAcademico, Long>{
    
}
