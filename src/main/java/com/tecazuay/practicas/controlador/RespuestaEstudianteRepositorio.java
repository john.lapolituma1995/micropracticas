
package com.tecazuay.practicas.controlador;


import com.tecazuay.practicas.modelo.RespuestaEstudiante;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RespuestaEstudianteRepositorio extends JpaRepository<RespuestaEstudiante, Long>{
    
}
