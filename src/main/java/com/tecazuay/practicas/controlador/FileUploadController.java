 package com.tecazuay.practicas.controlador;

import com.tecazuay.practicas.guardar.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.stream.Stream;

@Controller
public class FileUploadController {

    private final StorageService storageService;

    @Autowired
    public FileUploadController(StorageService storageService) {

        this.storageService = storageService;
    }

    @GetMapping("/")
    @ResponseBody
    @CrossOrigin
    public Stream<Path> listUploadedFiles() throws IOException {

    	Stream<Path> sp = storageService.loadAll();
    	
        return sp;
    }

    @GetMapping("/file/{filename:.+}")
    @ResponseBody
    @CrossOrigin
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

    @PostMapping("/")
    @ResponseBody
    @CrossOrigin
    public Object handleFileUpload(@RequestParam("file") MultipartFile file
    ) {

        //storageService.changePath("primera");
        String nombreArchivoRandom = String.format("%s-%s", java.util.UUID.randomUUID(), file.getOriginalFilename());
        storageService.setNombre(nombreArchivoRandom);

        storageService.store(file);
// redirectAttributes.addFlashAttribute("message",
        //       "You successfully uploaded " + file.getOriginalFilename() + "!");
        HashMap<String, Object> hmap = new HashMap<String, Object>();
        HashMap<String, Object> datos = new HashMap<String, Object>();

        /**/
        hmap.put("codigo", "001");
        hmap.put("respuesta", "Archivo subido correctamente");
        datos.put("archivo", nombreArchivoRandom);
        datos.put("rutaDir", storageService.getPath());
        datos.put("urlarchivo", "files/" + nombreArchivoRandom);
        
        hmap.put("datos", datos);
        return hmap;
    }
    @DeleteMapping("/file/{filename:.+}")
    @ResponseBody
    @CrossOrigin
    public void deleteFile(@PathVariable String filename ) {
    	System.out.println(filename);
    	storageService.deleteOne(filename);
    }

}
