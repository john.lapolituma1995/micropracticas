
package com.tecazuay.practicas.controlador;


import com.tecazuay.practicas.modelo.RespuestaEmpresaDesierta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RespuestaEmpresaDesiertaRepositorio extends JpaRepository<RespuestaEmpresaDesierta, Long>{
    
}
