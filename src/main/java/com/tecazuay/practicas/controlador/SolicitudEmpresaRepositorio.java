
package com.tecazuay.practicas.controlador;


import com.tecazuay.practicas.modelo.SolicitudEmpresa;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author casa
 */
public interface SolicitudEmpresaRepositorio extends JpaRepository<SolicitudEmpresa, Long>{
    
}
