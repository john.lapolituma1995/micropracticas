
package com.tecazuay.practicas.controlador;


import com.tecazuay.practicas.modelo.PlanificacionTutorAcademico;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author casa
 */
public interface PlanificaciontutorARepositorio extends JpaRepository<PlanificacionTutorAcademico, Long>{
    
}
