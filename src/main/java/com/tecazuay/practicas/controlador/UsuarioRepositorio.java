
package com.tecazuay.practicas.controlador;


import com.tecazuay.practicas.modelo. Usuario;
// import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author casa
 */
public interface UsuarioRepositorio extends JpaRepository< Usuario, Long>{
       
       @Query(value = "select a from Usuario a where a.usuario = :usuario")
                   Usuario validarUsuario(@Param("usuario") String usuario);
     ////Optional<Usuario> findByResetToken(String resetearToken);
}
