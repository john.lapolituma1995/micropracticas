
package com.tecazuay.practicas.controlador;


import com.tecazuay.practicas.modelo.CertificadoEmpresa;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author casa
 */
public interface CertificadoRepositorio extends JpaRepository<CertificadoEmpresa, Long>{
    
}
