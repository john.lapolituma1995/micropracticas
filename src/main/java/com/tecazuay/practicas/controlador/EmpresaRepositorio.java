
package com.tecazuay.practicas.controlador;


import com.tecazuay.practicas.modelo.Empresa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmpresaRepositorio extends JpaRepository<Empresa, Long>{
    
}
