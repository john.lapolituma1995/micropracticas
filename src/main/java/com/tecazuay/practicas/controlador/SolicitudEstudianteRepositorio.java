
package com.tecazuay.practicas.controlador;


import com.tecazuay.practicas.modelo.SolicitudEstudiante;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author casa
 */
public interface SolicitudEstudianteRepositorio extends JpaRepository<SolicitudEstudiante, Long>{
    
}
