
package com.tecazuay.practicas.controlador;


import com.tecazuay.practicas.modelo.EvaluacionTutorEmpresarial;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author casa
 */
public interface EvaluacionTERepositorio extends JpaRepository<EvaluacionTutorEmpresarial, Long>{
    
}
