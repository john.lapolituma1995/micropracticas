
package com.tecazuay.practicas.controlador;


import com.tecazuay.practicas.modelo.Perfil;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author casa
 */
public interface PerfilRepositorio extends JpaRepository<Perfil, Long>{
    
}
