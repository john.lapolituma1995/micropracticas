
package com.tecazuay.practicas.controlador;


import com.tecazuay.practicas.modelo.InformeTutorAcademico;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author casa
 */
public interface InformetutorARepositorio extends JpaRepository<InformeTutorAcademico, Long>{
    
}
