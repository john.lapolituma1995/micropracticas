
package com.tecazuay.practicas.controlador;


import com.tecazuay.practicas.modelo. TutorEmpresarial;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author casa
 */
public interface  TutorEmpresarialRepositorio extends JpaRepository< TutorEmpresarial, Long>{
    
}
