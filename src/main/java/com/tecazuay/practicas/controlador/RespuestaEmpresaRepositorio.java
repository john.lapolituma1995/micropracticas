
package com.tecazuay.practicas.controlador;


import com.tecazuay.practicas.modelo.RespuestaEmpresa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RespuestaEmpresaRepositorio extends JpaRepository<RespuestaEmpresa, Long>{
    
}
