
package com.tecazuay.practicas.controlador;


import com.tecazuay.practicas.modelo.Reunion;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author casa
 */
public interface ReunionRepositorio extends JpaRepository<Reunion, Long>{
    
}
